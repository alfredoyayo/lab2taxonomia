package com.example.amlab2taxonomia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.amlab2taxonomia.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        events()
    }

    private fun events() {
        binding.btnVerify.setOnClickListener { verifyYear() }
    }

    private fun verifyYear(){
        binding.apply {
            val year = edtYear.text.toString().trim().toInt()
            when(year){
                in 1994 .. 2010 -> {
                    tvGeneration.text = "Generación Z"
                    tvPopulation.text = "7.800.000"
                    ivTrait.setImageResource(R.drawable.irrevencia)
                }
                in 1981 .. 1993 -> {
                    tvGeneration.text = "Generación Y"
                    tvPopulation.text = "7.200.000"
                    ivTrait.setImageResource(R.drawable.frustracion)
                }
                in 1969 .. 1980 -> {
                    tvGeneration.text = "Generación X"
                    tvPopulation.text = "9.300.000"
                    ivTrait.setImageResource(R.drawable.obsesion)
                }
                in 1949 .. 1968 -> {
                    tvGeneration.text = "Baby Boom"
                    tvPopulation.text = "12.200.000"
                    ivTrait.setImageResource(R.drawable.ambicion)
                }
                in 1930 .. 1948 -> {
                    tvGeneration.text = "Silent Generation"
                    tvPopulation.text = "6.300.000"
                    ivTrait.setImageResource(R.drawable.austeridad)
                }
                else ->{
                    tvGeneration.text = "[Generacion]"
                    tvPopulation.text = "[Poblacion]"
                    ivTrait.setImageResource(android.R.drawable.ic_menu_gallery)
                    Toast.makeText(this@MainActivity, "No existe informacion para ese año", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}